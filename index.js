const Caller = require("./caller");
const Routes = require("./routes");

const getAuthHeader = (auth, key) => ({
  authorization: auth,
  "Communication-Key": key,
});

const getOptions = async (index, auth, key, code) => ({
  route: Routes[index],
  method: "GET",
  headers: getAuthHeader(auth, key),
  params: code ? code : null,
});

module.exports = {
  getCountryList: async (auth, key) =>
    await Caller(await getOptions("country", auth, key)),

  getEnvironmentTypesList: async (auth, key) =>
    await Caller(await getOptions("environmentType", auth, key)),

  getProvidersCategoryList: async (auth, key) =>
    await Caller(await getOptions("providerCategory", auth, key)),

  getProvidersListByCountry: async (auth, key, code) =>
    await Caller(await getOptions("countryProviders", auth, key, [code])),

  getProvidersListByCategory: async (auth, key, code) =>
    await Caller(await getOptions("categoryProviders", auth, key, [code])),

  getProductsListByProvider: async (auth, key, code) =>
    await Caller(await getOptions("providerProducts", auth, key, [code])),

  getMethodsListByProduct: async (auth, key, code) =>
    await Caller(await getOptions("productMethods", auth, key, [code])),

  getMethodsListByProductAndEnvironment: async (
    auth,
    key,
    productCode,
    envTypeCode
  ) =>
    await Caller(
      await getOptions("providerProductMethod", auth, key, [
        productCode,
        envTypeCode,
      ])
    ),

  getMethodToExecute: async (auth, key, code) => {
    let R = await Caller(
      await getOptions("providerProductMethodToExecute", auth, key, [code])
    );

    R.fields = R.ProviderApiProducts[0].ProviderProductMethods[0].Fields;
    delete R.ProviderApiProducts[0].ProviderProductMethods[0].Fields;
    R.providerProductMethod =
      R.ProviderApiProducts[0].ProviderProductMethods[0];
    delete R.ProviderApiProducts[0].ProviderProductMethods;

    R.providerProductMethod.enviornmentAvailability =
      R.providerProductMethod.EnviornmentAvailability.name;
    R.providerProductMethod.enviornmentType =
      R.providerProductMethod.EnviornmentType.name;
    R.providerProductMethod.methodType =
      R.providerProductMethod.MethodType.name;

    delete R.providerProductMethod.EnviornmentAvailability;
    delete R.providerProductMethod.EnviornmentType;
    delete R.providerProductMethod.MethodType;

    R.providerApiProduct = R.ProviderApiProducts[0];
    delete R.ProviderApiProducts;

    let MFs = R.fields.map((item, key) => {
      return {
        code: item.code,
        label: item.title,
        name: item.key,
        type: item.FieldType.name,
        category: item.MethodRequestOption.name,
        childOf: item.childOf,
        options: item.FieldPossibleValues,
        optionlabel: "value",
        optionvalue: "value",
      };
    });
    R.fields = MFs;

    console.log("THis is method ==> ", R);
    return R;
  },
};
