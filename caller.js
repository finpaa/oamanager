const { API_BASE_URL } = require("./constants.js");

module.exports = async ({ route, method, params, headers }) => {
  var url = API_BASE_URL + route;

  if (params && params !== null) url += "/" + params.join("/");

  // Request Options
  var options = { method, headers };

  return fetch(url, options)
    .then(async (response) => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json();
      } else if (contentType && contentType.indexOf("application/pdf") !== -1) {
        return response.blob();
      } else {
        return response.text();
      }
    })
    .catch((err) => {
      console.log("API ERROR", err);
      return false;
    });
};
